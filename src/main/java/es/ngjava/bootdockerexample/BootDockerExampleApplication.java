package es.ngjava.bootdockerexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootDockerExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootDockerExampleApplication.class, args);
	}
}
